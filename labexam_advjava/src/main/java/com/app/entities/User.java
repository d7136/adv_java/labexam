package com.app.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "user")
@Getter
@Setter
@ToString
public class User extends BaseEntity {
	
	@Column(length = 20, unique = true)
	@NotBlank
	private String userName;
	@Column(length = 20)
	@NotBlank
	private String password;
	@Enumerated(EnumType.STRING)
	private RoleEnum role;
	

}
