package com.app.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "product")
@Getter
@Setter
@ToString
public class Product extends BaseEntity {
	@Column(length = 20)
	@NotBlank
	private String name;
	@Column(length = 100)
	@NotBlank
	private String desc;
	@NotNull
	private int qnty;
	@NotNull
	private double price;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "u_id",nullable = false)
	private User user;
}
