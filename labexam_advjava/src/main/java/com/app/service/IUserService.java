package com.app.service;

import com.app.entities.User;

public interface IUserService {

	User getUserDtl(String userName, String password);

}
