package com.app.service;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.dao.ProductRepository;
import com.app.entities.Product;
import com.app.entities.User;

@Service
@Transactional
public class ProductServiceImpl implements IProductService {

	@Autowired
	private ProductRepository proRepo;
	
	
	public List<Product> getAllPrdcts() {
		
		return proRepo.findAll();
	}


	public Product addProduct(@Valid Product pro, HttpSession session) {
		User usr = (User) session.getAttribute("user_dtl");
		pro.setUser(usr);
		
		return proRepo.save(pro);
	}


	public void deleteProduct(long id) {
		
		proRepo.deleteById(id);
	}

}
