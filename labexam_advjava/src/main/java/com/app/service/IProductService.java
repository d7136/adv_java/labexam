package com.app.service;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import com.app.entities.Product;

public interface IProductService {

	List<Product> getAllPrdcts();
	Product addProduct(@Valid Product pro, HttpSession session);
	void deleteProduct(long id);
}
