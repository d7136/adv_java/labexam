package com.app.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.app.entities.Product;
import com.app.service.ProductServiceImpl;

@Controller
@RequestMapping("/admin")
public class AdminController {
	
	@Autowired
	private ProductServiceImpl proRepo;
	
	@GetMapping("/products")
	public String showProducts(HttpSession session, Model map, Product pro) {
		
		map.addAttribute("product",proRepo.getAllPrdcts());
		return "/admin/product";
	}
}
