package com.app.controller;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.app.entities.Product;
import com.app.service.ProductServiceImpl;

@Controller
@RequestMapping("/user")

public class UserController {
	
	@Autowired
	private ProductServiceImpl proRepo;

	@GetMapping("/products")
	public String showProducts(HttpSession session, Model map, Product pro) {
		
		map.addAttribute("product",proRepo.getAllPrdcts());
		return "/user/product";
	}
	
	
	@GetMapping("/add")
	public String addProductForm(HttpSession session, Model map, Product pro) {
				
		return "/user/product_form";
	}
	
	@PostMapping("/add")
	public String processProductForm(HttpSession session, Model map, @Valid Product pro) {
				
		System.out.println("new product added "+proRepo.addProduct(pro, session));
		return "redirect:/user/product";
	}
	
	@GetMapping("/delete")
	public String deleteProduct(HttpSession session, Model map, long Id) {
				
		proRepo.deleteProduct(Id);
		return "redirect:/user/product";
	}
	
}
