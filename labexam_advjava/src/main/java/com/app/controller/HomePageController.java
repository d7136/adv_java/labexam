package com.app.controller;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.app.entities.RoleEnum;
import com.app.entities.User;
import com.app.service.IUserService;

@Controller
@RequestMapping("/")
public class HomePageController {
	
	@Autowired
	private IUserService userService;
	
	@GetMapping
	public String loginPage(User user) {
		System.out.println("in login method");
		return "/home/login";
	}
	
	@PostMapping
	public String processLoginPage(@Valid User user,HttpSession session, Model map) {
		System.out.println("in process login method");
		session.setAttribute("user_dtl", user);
		String userName = user.getUserName();
		String password = user.getPassword();
		User usr=userService.getUserDtl(userName,password);
		
		if (usr.getRole()==RoleEnum.ADMIN) {
			return "redirect:/user/products";
		}else if(usr.getRole()==RoleEnum.USER) {
			return "redirect:/admin/products";
		}
				
		return "/home/login";
	}

	@GetMapping
	public String logoutPage(User user,HttpSession session, Model map) {
		System.out.println("in logout method");
		User usr = (User) session.getAttribute("user_dtl");
		
		map.addAttribute("user", usr);
		session.invalidate();
		System.out.println("in logout method");
		return "/home/logout";
	}
}
