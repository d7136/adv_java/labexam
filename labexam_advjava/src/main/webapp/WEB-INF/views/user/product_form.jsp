<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<form:form modelAttribute="product">
		<table style="background-color: lightgrey; margin: auto">
			<tr>
				<td>Product Name </td>
				<td><form:input path="name" /></td>
				<td><form:errors path="name" /></td>
			</tr>
			<tr>
				<td>Product Description</td>
				<td><form:input path="desc" /></td>
				<td><form:errors path="desc" /></td>
			</tr>
			<tr>	
				<td>Product Quantity</td>
				<td><form:input path="qnty" /></td>
				<td><form:errors path="qnty" /></td>
			</tr>
			<tr>
				<td>Product Price</td>
				<td><form:input path="price" /></td>
				<td><form:errors path="price" /></td>
			</tr>
			<tr>
				<td><input type="submit" value="Add" /></td>
			</tr>
		</table>
	</form:form>
</body>
</html>