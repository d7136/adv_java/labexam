<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<style>
table, th, td, tr {
	border: 2px solid black;
	border-collapse: collapse;
	border-style: double;
}

h5 {
	text-align: center;
}
</style>
<title>Insert title here</title>
</head>
<body>
	<table style="background-color: lightgrey; margin: auto">
		<h5>List of Products</h5>
		<tr style="border: 1px solid black;">
			<th>Product Id</th>
			<th>Product Name</th>
			<th>Description</th>
			<th>Quantity</th>
			<th>Price</th>
			<th>Delete</th>
		</tr>
		<c:forEach var="cntry" items="${requestScope.product}">
			<tr>
				<td>${product.id}</td>
				<td>${product.name}</td>
				<td>${product.desc}</td>
				<td>${product.qnty}</td>
				<td>${product.price}</td>
				<td><a
					href="<spring:url value='/user/delete?Id=${product.id}'/>">Delete
						Product</a></td>
			</tr>
		</c:forEach>
	</table>
	<h5>
		<a href="<spring:url value='/user/add'/>">Add New Product</a>
	</h5>
	<h5>
		<a href="<spring:url value='/logout'/>">LogOut</a>
	</h5>

</body>
</html>